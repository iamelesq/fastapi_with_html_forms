# fastapi imports
import imp
from urllib import request, response
from fastapi import FastAPI, HTTPException, Path, Query, Request, status, Body, Form
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.encoders import jsonable_encoder
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

# genral imports
from pydantic import BaseModel, Field
from typing import Optional, List, Dict

# local imports
from db import cars


templates = Jinja2Templates(directory="templates")


# uses pydantic BaseModel. Fields are made to be optinal
# to support the editing feature.
class Car(BaseModel):
    make: Optional[str]
    model: Optional[str]
    year: Optional[int] = Field(..., ge=1970, lt=2023)
    price: Optional[float]
    engine: Optional[str] = "V4"
    autonomous: Optional[bool]
    sold: Optional[List[str]]


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

# redirected to /cars
@app.get("/", response_class=RedirectResponse)
async def root(request: Request):
    return RedirectResponse(url="/cars")


# home route
@app.get("/cars", response_class=HTMLResponse)
async def get_all_cars(
    request: Request, number: Optional[str] = Query("10", max_length=3)
):
    response = []
    for id, car in list(cars.items())[: int(number)]:
        response.append((id, car))
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "cars": response, "title": "Auto Lookup | Home"},
    )


# search
@app.post("/search", response_class=RedirectResponse)
async def search_cars(id: str = Form(...)):
    return RedirectResponse(url="/cars/" + id, status_code=status.HTTP_302_FOUND)


# return a car for a matching ID or return a 404
@app.get("/cars/{id}", response_class=HTMLResponse)
async def get_car_by_id(request: Request, id: int = Path(..., ge=0, lt=1000)):
    car = cars.get(id)
    response = templates.TemplateResponse(
        "search.html", {"request": request, "car": car, "id": id, "title": "Search"}
    )

    if not car:
        response.status_code = status.HTTP_404_NOT_FOUND
    return response


# gets the create form
@app.get("/create", response_class=HTMLResponse)
async def create_car(request: Request):
    return templates.TemplateResponse(
        "create.html", {"request": request, "title": "Auto Lookup | Add new inventory"}
    )


# post method for the create form accepts in the values from
# the html form. A Car model is made from the field values
# the list of cars is travesed to find the next viable ID and
# the car model is appended to the dictionary.
@app.post("/cars", status_code=status.HTTP_201_CREATED)
async def create_new_car(
    make: Optional[str] = Form(...),
    model: Optional[str] = Form(...),
    year: Optional[int] = Form(...),
    price: Optional[float] = Form(...),
    engine: Optional[str] = Form(...),
    autonomous: Optional[bool] = Form(...),
    sold: Optional[List[str]] = Form(None),
    min_id: Optional[int] = Body(0),
):
    body_cars = [
        Car(
            make=make,
            model=model,
            year=year,
            price=price,
            engine=engine,
            autonomous=autonomous,
            sold=sold,
        )
    ]
    if len(body_cars) < 1:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="No cars to add"
        )
    min_id = len(cars.values()) + min_id
    for car in body_cars:
        while cars.get(min_id):
            min_id += 1
        cars[min_id] = car
        min_id += 1
    return RedirectResponse(url="/cars", status_code=status.HTTP_302_FOUND)


# edit looks for a matching car, if one is not found then the
# returned template is the search form. Where a match if found
# the /edit form is returned and the values associated with the
# record are prepopulated in the fields.
@app.get("/edit", response_class=HTMLResponse)
async def edit_car(request: Request, id: int = Query(...)):
    car = cars.get(id)

    if not car:
        return templates.TemplateResponse(
            "search.html",
            {
                "request": request,
                "car": car,
                "id": id,
                "title": "Auto Lookup | Edit car Details",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    return templates.TemplateResponse(
        "edit.html",
        {
            "request": request,
            "car": car,
            "id": id,
            "title": "Auto Lookup | Edit car Details",
        },
        status_code=status.HTTP_200_OK,
    )


# pass in the request and the form values. Finds the car from
# the list that matches the id or sends a 404. Once matched it
# perfors the operations and conversions required to be in a
# fit state to be submitted to the dictionary
@app.post("/cars/{id}", response_class=HTMLResponse)
async def update_car(
    request: Request,
    id: int,
    make: Optional[str] = Form(None),
    model: Optional[str] = Form(None),
    year: Optional[int] = Form(None),
    price: Optional[float] = Form(None),
    engine: Optional[str] = Form(None),
    autonomous: Optional[bool] = Form(None),
    sold: Optional[List[str]] = Form(None),
):
    existing_car = cars.get(id)
    if not existing_car:
        return templates.TemplateResponse(
            "search.html",
            {
                "request": request,
                "car": car,
                "id": id,
                "title": "Auto Lookup | Edit car Details",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )

    # unpack into a model, convert to a dictionary excluding the
    # unset values as not required. Copy object in update mode
    # replacing the values of existing fields provided and then
    # convert back to a json object and assign to the list.
    existing_car = Car(**dict(existing_car))
    car = Car(
        make=make,
        model=model,
        year=year,
        price=price,
        engine=engine,
        autonomous=autonomous,
        sold=sold,
    )
    new_car = car.dict(exclude_unset=True)
    new_car = existing_car.copy(update=new_car)
    cars[id] = jsonable_encoder(new_car)
    response = {}
    response[id] = cars[id]
    return RedirectResponse(url="/cars", status_code=status.HTTP_302_FOUND)


# simply deletes a match from the dict
@app.get("/delete/{id}", response_class=RedirectResponse)
async def delete_car(request: Request, id: int = Path(...)):
    if not cars.get(id):
        return templates.TemplateResponse(
            "search.html",
            {
                "request": request,
                "id": id,
                "title": "Auto Lookup | Edit car Details",
            },
            status_code=status.HTTP_404_NOT_FOUND,
        )
    del cars[id]
    return RedirectResponse(url="/cars")
